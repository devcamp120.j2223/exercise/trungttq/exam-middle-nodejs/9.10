const express = require("express");
const {middlewareTimeLog, middelwareUrl} = require("./app/middleware/midleware");

const path = require("path");
const app =  express();
const port = 8000;

app.use(express.static("views"));
app.use(middlewareTimeLog);
app.use(middelwareUrl);

app.get("/", (req, res) => {
    let url = "https://getbootstrap.com/docs/4.0/examples/sign-in/"
    res.status(200).sendFile(path.join(__dirname + "/views/Signin Template for Bootstrap.html"))
})

app.listen(port, () => {
    console.log(`port running at port ${port}`);

})